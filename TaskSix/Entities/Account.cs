﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSix.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime OpeningDate { get; set; }
        public decimal CashAll { get; set; }
        public int UserId { get; set; }
    }
}
