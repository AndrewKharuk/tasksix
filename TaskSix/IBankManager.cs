﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSix.Entities;
using TaskSix.ViewModels;

namespace TaskSix
{
    public interface IBankManager
    {
        User GetUserByLoginAndPassword(string login, string pass);
        UserWithAccounts GetAccountsByUserLogin(string login);
        UserWithHistoryGroupByAccount GetAccountsWithHistoryByUserId(int id);
        IEnumerable<OperationsWithUsers> GetAllInputOperationsWithUsers();
        IEnumerable<UsersWithCashAllBySum> GetUsersBySum(decimal sum);
    }
}
