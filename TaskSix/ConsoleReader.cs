﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskSix.Data;
using TaskSix.Entities;
using TaskSix.ViewModels;

namespace TaskSix
{
    public class ConsoleReader : IReader
    {
        private readonly IBankManager _manager;

        public ConsoleReader(IContext db)
        {
            _manager = new BankManager(db);
        }

        public void ShowUserByLoginAndPassword(string login, string pass)
        {
            var result = _manager.GetUserByLoginAndPassword(login, pass);

            Console.WriteLine($"1. Вывод информации о заданном аккаунте (пользователе) по логину и паролю: {login} - {pass}");
            Console.WriteLine($"Id: {result.Id}\r\nПользователь: {result.SurName} {result.FirstName} {result.MiddleName}\r\nТелефон: {result.Phone}\r\nПаспорт: {result.Pasport}\r\nДата регистрации: {result.RegistrationDate}\r\nЛогин и пароль: {result.Login} - {result.Password}\r\n");
        }


        public void ShowAccountsByUserLogin(string login)
        {
            UserWithAccounts result = _manager.GetAccountsByUserLogin(login);

            Console.WriteLine($"2.Вывод данных о всех счетах заданного пользователя (с логином {login})");
            Console.WriteLine($"Пользователь {result.User.SurName} {result.User.FirstName} {result.User.MiddleName}\r\nВсе счета:");
            foreach (var item in result.Accounts)
            {
                Console.WriteLine($"Счет №{item.Id}, дата открытия: {item.OpeningDate.ToShortDateString()}, сумма: {item.CashAll}");
            }
            Console.WriteLine();
        }

        public void ShowAccountsWithHistoryByUserId(int id)
        {
            UserWithHistoryGroupByAccount result = _manager.GetAccountsWithHistoryByUserId(id);

            Console.WriteLine($"3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту (userId = {id})");
            Console.WriteLine($"Пользователь {result.User.SurName} {result.User.FirstName} {result.User.MiddleName}\r\nВсе счета:");
            foreach (var grouping in result.History)
            {
                Console.WriteLine($"Счет №{grouping.Key}, дата открытия: {grouping.First().OpeningDate.ToShortDateString()}, всего на счете: {grouping.First().CashAll}:");
                foreach (var item in grouping)
                {
                    Console.WriteLine($"Дата операции: {item.OperationDate},тип: {item.OperationType}, сумма: {item.CashSum}");
                }
                Console.WriteLine();
            }
        }

        public void ShowAllInputOperationsWithUsers()
        {
            IEnumerable<OperationsWithUsers> result = _manager.GetAllInputOperationsWithUsers();

            Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            foreach (OperationsWithUsers item in result)
            {
                Console.WriteLine($"Дата операции: {item.Date}, пользователь: {item.SurName} {item.FirstName} {item.MiddleName}, № счета: {item.AccountId}, сумма: {item.Sum}");
            }
            Console.WriteLine();
        }

        public void ShowUsersBySum(int sum)
        {
            IEnumerable<UsersWithCashAllBySum> result = _manager.GetUsersBySum(sum);

            Console.WriteLine("5.Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)");
            Console.WriteLine($"Контрольная сумма: {sum}");

            foreach (var item in result)
            {
                Console.WriteLine($"Пользователь: {item.SurName} {item.FirstName} {item.MiddleName}, № счета: {item.AccountId}, сумма: {item.CashAll}");
            }
            Console.WriteLine();
        }
    }
}
