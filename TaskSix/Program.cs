﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSix.Data;

namespace TaskSix
{
    class Program
    {
        static void Main(string[] args)
        {
            IContext db = new ListContext();
            IReader reader = new ConsoleReader(db);

            reader.ShowUserByLoginAndPassword("snow", "111");
            reader.ShowAccountsByUserLogin("snow");
            reader.ShowAccountsWithHistoryByUserId(1);
            reader.ShowAllInputOperationsWithUsers();
            reader.ShowUsersBySum(10000);

            Console.ReadKey();
        }
    }
}
