﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSix.Entities;

namespace TaskSix.Data
{
    public interface IContext
    {
        List<User> Users { get; }
        List<Account> Accounts { get; }
        List<History> Histories { get; }
    }
}
