﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskSix.Data;
using TaskSix.Entities;
using TaskSix.ViewModels;

namespace TaskSix
{
    public class BankManager : IBankManager
    {
        private readonly IContext _db;

        public BankManager(IContext context)
        {
            _db = context;
        }

        //1. Вывод информации о заданном аккаунте (пользователе) по логину и паролю
        public User GetUserByLoginAndPassword(string login, string pass)
        {        
            return _db.Users.FirstOrDefault(a => a.Login == login && a.Password == pass);
        }

        //2. Вывод данных о всех счетах заданного пользователя
        public UserWithAccounts GetAccountsByUserLogin(string login)
        {
            var user = _db.Users.FirstOrDefault(u => u.Login == login);
            var accounts = _db.Accounts.Where(a => a.UserId == user.Id).ToArray();
           
            return new UserWithAccounts { User = user, Accounts = accounts};
        }

        //3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        public UserWithHistoryGroupByAccount GetAccountsWithHistoryByUserId(int id)
        {
            var user = _db.Users.FirstOrDefault(u => u.Id == id);

            var query = (from account in (_db.Accounts.Where(a => a.UserId == id))
                         join history in _db.Histories
                             on account.Id equals history.AccountId
                         select new AccountWithHistory { Id = account.Id, 
                                                            OpeningDate = account.OpeningDate,
                                                            CashAll = account.CashAll, 
                                                            OperationDate = history.OperationDate, 
                                                            OperationType = history.OperationType, 
                                                            CashSum = history.CashSum })
                         .GroupBy(x => x.Id).ToArray();

            return new UserWithHistoryGroupByAccount { User = user, History = query };
        }

        //4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
        public IEnumerable<OperationsWithUsers> GetAllInputOperationsWithUsers()
        {
            var query = (from inputOperations in _db.Histories.Where(h => h.OperationType == OperationType.InputCash)
                        join account in _db.Accounts
                            on inputOperations.AccountId equals account.Id
                        join user in _db.Users
                            on account.UserId equals user.Id
                        select new OperationsWithUsers{ Date = inputOperations.OperationDate, 
                                                        Sum = inputOperations.CashSum, 
                                                        AccountId = account.Id, 
                                                        SurName = user.SurName, 
                                                        FirstName = user.FirstName, 
                                                        MiddleName = user.MiddleName}).OrderBy(o => o.Date);

            return query.ToArray();
        }

        //5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)
        public IEnumerable<UsersWithCashAllBySum> GetUsersBySum(decimal sum)
        {
            var query = from account in _db.Accounts.Where(a => a.CashAll > sum)
                        join user in _db.Users on account.UserId equals user.Id
                        select new UsersWithCashAllBySum { SurName = user.SurName, 
                                                            FirstName = user.FirstName,
                                                            MiddleName = user.MiddleName,
                                                            AccountId = account.Id,
                                                            CashAll = account.CashAll };

            return query.ToArray();
        }
    }
}
