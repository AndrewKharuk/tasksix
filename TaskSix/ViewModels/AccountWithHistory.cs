﻿using System;
using TaskSix.Entities;

namespace TaskSix.ViewModels
{
    public class AccountWithHistory
    {
        public int Id { get; set; }
        public DateTime OpeningDate { get; set; }
        public decimal CashAll { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal CashSum { get; set; }
    }
}
