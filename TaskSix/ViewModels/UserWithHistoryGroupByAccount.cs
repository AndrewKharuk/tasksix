﻿using System.Collections.Generic;
using System.Linq;
using TaskSix.Entities;

namespace TaskSix.ViewModels
{
    public class UserWithHistoryGroupByAccount
    {
        public User User { get; set; }
        public IEnumerable<IGrouping<int, AccountWithHistory>> History { get; set; }
    }
}
