﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSix.ViewModels
{
    public class UsersWithCashAllBySum
    {
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string MiddleName { get; set; }
        public int AccountId { get; set; }
        public decimal CashAll { get; set; }
    }
}
