﻿using System.Collections.Generic;
using TaskSix.Entities;

namespace TaskSix.ViewModels
{
    public class UserWithAccounts
    {
        public User User { get; set; }
        public IEnumerable<Account> Accounts { get; set; }
    }
}
