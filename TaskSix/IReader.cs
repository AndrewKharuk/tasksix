﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSix
{
    public interface IReader
    {
        void ShowUserByLoginAndPassword(string login, string pass);
        void ShowAccountsByUserLogin(string login);
        void ShowAccountsWithHistoryByUserId(int id);
        void ShowAllInputOperationsWithUsers();
        void ShowUsersBySum(int sum);
    }
}
